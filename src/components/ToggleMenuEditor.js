import { connect } from 'react-redux';
import React from 'react';
import {toggleMenuVisibility}from '../actions';

const ToggleMenuEditor= (props)=>{
	const buttonText=props.isEditorOpen?"close":"open";
	return (
		<button 
			className="toggle-menu-editor" 
			onClick={props.toggleMenuVisibility}>
		{buttonText}
		</button>
		)
}

const mapStateToProps=(state)=>{
	return {
		isEditorOpen:state.isEditorOpen
	}
}

const mapDispatchToProps=(dispatch)=>{
	return {
		toggleMenuVisibility:()=>dispatch(toggleMenuVisibility())
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(ToggleMenuEditor)