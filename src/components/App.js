import React, { Component } from 'react';
import Menu from './Menu';
import MenuEditor from './MenuEditor';

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			items: [],
			isEditorOpen:false
		};
	}

	onAddItem(item){		
		this.setState({
			items:[...this.state.items,item]
		})
	}

	toggleMenuEditor(){

		this.setState({
			isEditorOpen:!this.state.isEditorOpen
		})
	}

	render() {

		return (
			<div className="container">				
				<Menu />				
				<MenuEditor />							
			</div>
			);
	}
}

export default App;
