import React from 'react';
import { connect } from 'react-redux';
import ToggleItemState from './ToggleItemState';
import {changeItemTitle,toggleItem} from '../actions';

const ItemEditor= (props)=>{
	let textarea;
	//let isDisabled=props.item.isVisible?"enable":"disabled";
	return (
		<div className="item-editor-container">
			<label className="item-editor-label">Item #{props.item.id}</label>
			<textarea 
				className="item-editor-textarea"
				disabled={!props.item.isVisible}
				onChange={()=>props.onTitleChange(textarea.value)} 
				value={props.item.title} 
				ref={el=>textarea=el}>
			</textarea>
			<ToggleItemState 
				isVisible={props.item.isVisible}
				onChangeState={()=>props.onChangeState(props.item.id)} />
		</div>)
}

const mapDispatchToProps=(dispatch,ownProps)=>{
	return {
		onTitleChange:(value)=>{
			dispatch(changeItemTitle(value,ownProps.item.id));
		},
		onChangeState:(id)=>{
			dispatch(toggleItem(id));
		}
	}
}

export default connect(null,mapDispatchToProps)(ItemEditor)