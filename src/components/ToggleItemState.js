import React from 'react';

export default (props)=>{
	const buttonText=props.isVisible?"remove":"restore";
	return (
		<button className="toggle-item-state"
			onClick={props.onChangeState}>
			{buttonText}
		</button>
		)
}