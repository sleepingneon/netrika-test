import { connect } from 'react-redux';
import React from 'react';
import ItemEditor from './ItemEditor';
import {addNewItem} from '../actions';

const MenuEditor= (props)=>{

	if(props.isVisible){

		var itemEditors=props.items.map((item)=>{
			return <ItemEditor item={item} key={item.id} />
		})
		return (
			<div className="panel menu-editor">			
				<div>{itemEditors}</div>
				<button onClick={props.onAddNew}>add new</button>
			</div>
			)
	}else{
		return null;		
	}
}

const mapStateToProps=(state)=>{
	return {
		isVisible:state.isEditorOpen,
		items:state.items
	}
}

let id=1;

const mapDispatchToProps=(dispatch)=>{
	return{
		onAddNew:()=>{
			dispatch(addNewItem(id++));
		}
	}
}

export default connect(mapStateToProps,mapDispatchToProps)(MenuEditor)