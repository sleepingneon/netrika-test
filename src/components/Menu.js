import { connect } from 'react-redux';
import React from 'react';
import ToggleMenuEditor from './ToggleMenuEditor';
import MenuItem from './MenuItem';

const Menu= (props)=>{
	var menuItems=props.items.map((item)=>{
		if(item.isVisible && item.title!==""){
			return <MenuItem key={item.id} item={item} />
		}
	})

	return (
		<div className="panel menu">
			<ToggleMenuEditor />
			{menuItems}
		</div>)
}

const mapStateToProps=(state)=>{
	return {
		items:state.items
	}
}

export default connect(mapStateToProps)(Menu)