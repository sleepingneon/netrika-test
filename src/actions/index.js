export const toggleMenuVisibility=()=> ({
	type:"TOGGLE_MENU_VISIBILITY"		
});

export const addNewItem=(id)=>({	
	type:"ADD_ITEM",
	newItem:{
		title:"",
		isVisible:true,
		id
	}			
})


export const changeItemTitle=(newTitle,id)=>({
	type:"CHANGE_ITEM_TITLE",
	newTitle,
	id
})

export const toggleItem=(id)=>({
	type:"TOGGLE_ITEM",
	id
})