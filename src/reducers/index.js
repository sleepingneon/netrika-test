import { combineReducers } from 'redux'

const itemsReducer = (state=[],action)=>{
	switch(action.type){
		case "ADD_ITEM":
			return [...state,action.newItem];
			break;
		case "TOGGLE_ITEM":
			return state.map((item)=>{
				if(item.id===action.id){
					return Object.assign({},item, {isVisible:!item.isVisible});
				}else{
					return item;
				}
			});
			break;
		case "CHANGE_ITEM_TITLE":
			return state.map((item)=>{
				if(item.id===action.id){
					return Object.assign({},item, {title:action.newTitle});
				}else{
					return item;
				}
			});
			break;
		default:
			return state;
			break;
	}

}

const isEditorOpenReducer=(state=false,action)=>{
	switch(action.type){
		case "TOGGLE_MENU_VISIBILITY":
			return !state;
			break;
		default:
			return state;
			break;
	}
}

export default combineReducers({
	items:itemsReducer,
	isEditorOpen:isEditorOpenReducer
})